import pytest

# 被测函数

def add(x, y):
    return x + y

 @pytest.mark.hebeu
def test_add_integer():
    print("开始计算")
    assert add(2, 2) == 4
    assert add(99, 0) == 99
    assert add(-99, 0) == -99
    assert add(49, -50) ==-1
    assert add(99, -99) == 0
    print("结束计算")

 @pytest.mark.hebeu
def test_add_float():
    print("开始计算")
    assert add(1.1, 2.2) == pytest.approx(3.3, 0.01)
    assert add(-5.55, 10.10) == pytest.approx(4.55, 0.01)
    assert add(99.99, 0.01) == pytest.approx(100, 0.01)
    assert add(-99.99, 0.01) == pytest.approx(-99.98, 0.01)
    assert add(50.50, -50.50) == pytest.approx(0, 0.01)
    assert add(99.99, -99.99) == pytest.approx(0, 0.01)
    print("结束计算")